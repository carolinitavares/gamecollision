package com.example.macstudent.tappyspaceshipclass;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Enemy {
    int xPosition;
    int yPosition;
    int direction;
    Bitmap image;
    private Rect hitBox;

    public Enemy(Context context, int x, int y) {
        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.alien_ship2);
        this.xPosition = x;
        this.yPosition = y;

        this.hitBox = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + this.image.getWidth(),
                this.yPosition + this.image.getHeight()
        );

    }

    public Rect getHitBox(){
        return this.hitBox;
    }

    public void updateEnemyPosition() {
        this.xPosition = this.xPosition - 15;

        this.hitBox.left = this.xPosition;
        this.hitBox.right = (this.xPosition + this.image.getWidth());
        //this.hitBox.top = this.yPosition;
        //this.hitBox.bottom = this.yPosition + this.image.getHeight();
    }


    public void setXPosition(int x) {
        this.xPosition = x;
    }
    public void setYPosition(int y) {
        this.yPosition = y;
    }
    public int getXPosition() {
        return this.xPosition;
    }
    public int getYPosition() {
        return this.yPosition;
    }

    public Bitmap getBitmap() {
        return this.image;
    }

}
